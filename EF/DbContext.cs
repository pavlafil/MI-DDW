﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebCrawler.EF
{
    class EfDbContext : DbContext
    {
        public DbSet<EFArticle> Articles { get; set; }

        public DbSet<EFPerson> Persons { get; set; }
        public DbSet<EFUrl> Urls { get; set; }

        public void Earse()
        {
            Articles.RemoveRange(Articles);
            Urls.RemoveRange(Urls);
            Persons.RemoveRange(Persons);
            SaveChanges();
        }
    }
}
