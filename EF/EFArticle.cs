﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebCrawler.EF
{
    class EFArticle
    {
        [Key]
        public int ID { get; set; }
        public string Header { get; set; }
        public EFUrl Url { get; set; }
        public string Content { get; set; }
        public List<EFPerson> Authors { get; set; }
        public DateTime Date { get; set; }
    }
}
