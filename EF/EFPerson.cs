﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebCrawler.EF
{
    class EFPerson
    {
        [Key]
        public int ID { get; set; }
        public string FName { get; set; }
        public string SName { get; set; }
        public string Role { get; set; }
        public string Desc { get; set; }
        public List<EFArticle> Articles { get; set; }
    }
}
