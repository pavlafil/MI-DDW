﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebCrawler
{
    static class Extensions
    {
        public static string Pretify(this string input)
        {
            return input.Replace("\n", "").Replace("\t", "").Trim();
        }
    }
}
