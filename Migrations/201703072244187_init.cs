namespace WebCrawler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EFArticles",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Header = c.String(),
                        Content = c.String(),
                        Url_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.EFUrls", t => t.Url_ID)
                .Index(t => t.Url_ID);
            
            CreateTable(
                "dbo.EFUrls",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Uri = c.String(maxLength: 255, unicode: false),
                    })
                .PrimaryKey(t => t.ID)
                .Index(t => t.Uri);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EFArticles", "Url_ID", "dbo.EFUrls");
            DropIndex("dbo.EFUrls", new[] { "Uri" });
            DropIndex("dbo.EFArticles", new[] { "Url_ID" });
            DropTable("dbo.EFUrls");
            DropTable("dbo.EFArticles");
        }
    }
}
