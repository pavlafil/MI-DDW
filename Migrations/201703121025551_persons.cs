namespace WebCrawler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class persons : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EFPersons",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FName = c.String(),
                        SName = c.String(),
                        Role = c.String(),
                        Desc = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.EFPersons");
        }
    }
}
