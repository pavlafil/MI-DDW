namespace WebCrawler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class authors : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EFPersons", "EFArticle_ID", c => c.Int());
            CreateIndex("dbo.EFPersons", "EFArticle_ID");
            AddForeignKey("dbo.EFPersons", "EFArticle_ID", "dbo.EFArticles", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EFPersons", "EFArticle_ID", "dbo.EFArticles");
            DropIndex("dbo.EFPersons", new[] { "EFArticle_ID" });
            DropColumn("dbo.EFPersons", "EFArticle_ID");
        }
    }
}
