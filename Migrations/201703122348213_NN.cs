namespace WebCrawler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NN : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.EFPersons", "EFArticle_ID", "dbo.EFArticles");
            DropIndex("dbo.EFPersons", new[] { "EFArticle_ID" });
            CreateTable(
                "dbo.EFPersonEFArticles",
                c => new
                    {
                        EFPerson_ID = c.Int(nullable: false),
                        EFArticle_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.EFPerson_ID, t.EFArticle_ID })
                .ForeignKey("dbo.EFPersons", t => t.EFPerson_ID, cascadeDelete: true)
                .ForeignKey("dbo.EFArticles", t => t.EFArticle_ID, cascadeDelete: true)
                .Index(t => t.EFPerson_ID)
                .Index(t => t.EFArticle_ID);
            
            DropColumn("dbo.EFPersons", "EFArticle_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.EFPersons", "EFArticle_ID", c => c.Int());
            DropForeignKey("dbo.EFPersonEFArticles", "EFArticle_ID", "dbo.EFArticles");
            DropForeignKey("dbo.EFPersonEFArticles", "EFPerson_ID", "dbo.EFPersons");
            DropIndex("dbo.EFPersonEFArticles", new[] { "EFArticle_ID" });
            DropIndex("dbo.EFPersonEFArticles", new[] { "EFPerson_ID" });
            DropTable("dbo.EFPersonEFArticles");
            CreateIndex("dbo.EFPersons", "EFArticle_ID");
            AddForeignKey("dbo.EFPersons", "EFArticle_ID", "dbo.EFArticles", "ID");
        }
    }
}
