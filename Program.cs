﻿using Abot.Crawler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebCrawler
{
    class Program
    {
        static void Main(string[] args)
        {
            //string url = "http://www.zive.cz/default.aspx?taglist=1";

            var context = new WebCrawler.EF.EfDbContext();
            var articles = context.Articles.Include("Url").Include("Authors").Take(200).ToList();

            var serialize = Newtonsoft.Json.JsonConvert.SerializeObject(articles);

            string url = "http://www.zive.cz/clanky/tyden-zive-microsoft-vypne-svuj-nejzbytecnejsi-web-socl/sc-3-a-186604/default.aspx";
            var solver = new Solver(url);
            solver.Solve();
        }

    }
}
