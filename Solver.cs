﻿using Abot.Crawler;
using Abot.Poco;
using AngleSharp.Dom;
using AngleSharp.Dom.Html;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using WebCrawler.EF;

namespace WebCrawler
{
    class Solver
    {
        private PoliteWebCrawler crawler { get; set; }
        private string url { get; set; }

        private EF.EfDbContext context { get; set; }

        public Solver(string url)
        {
            this.url = url;
            crawler = new PoliteWebCrawler();
            crawler.PageCrawlStartingAsync += crawler_ProcessPageCrawlStarting;
            crawler.PageCrawlCompletedAsync += crawler_ProcessPageCrawlCompleted;
            crawler.PageCrawlDisallowedAsync += crawler_PageCrawlDisallowed;
            crawler.PageLinksCrawlDisallowedAsync += crawler_PageLinksCrawlDisallowed;

            context = new EfDbContext();
            context.Earse();
        }

        public void Solve()
        {
            CrawlResult result = crawler.Crawl(new Uri(url));

            if (result.ErrorOccurred)
                Console.WriteLine("Crawl of {0} completed with error: {1}", result.RootUri.AbsoluteUri, result.ErrorException.Message);
            else
                Console.WriteLine("Crawl of {0} completed without error.", result.RootUri.AbsoluteUri);
        }

        private void crawler_ProcessPageCrawlStarting(object sender, PageCrawlStartingArgs e)
        {
            PageToCrawl pageToCrawl = e.PageToCrawl;
            Console.WriteLine("About to crawl link {0} which was found on page {1}", pageToCrawl.Uri.AbsoluteUri, pageToCrawl.ParentUri.AbsoluteUri);
            pageToCrawl.Uri = new Uri(pageToCrawl.Uri.OriginalString.Split('?').First());
        }

        private void Extract(PageCrawlCompletedArgs page, IHtmlDocument document)
        {
            lock (context)
            {
                if (page.CrawledPage.Uri.ToString().Contains(".cz/clanky/") &&
                    document.QuerySelector(".ar-detail") != null &&
                    document.QuerySelector(".ar-detail h1") != null &&
                    document.QuerySelector(".ar-date") != null &&
                    context.Urls.FirstOrDefault(x => x.Uri == page.CrawledPage.Uri.OriginalString) == null)
                {
                    var authors = new List<EFPerson>();

                    var rawAuthors = document.QuerySelectorAll(".ar-info .ar-author a");
                    foreach (var a in rawAuthors)
                    {
                        var name = a.TextContent.Split(' ');
                        if (name.Length >= 2)
                        {
                            var fname = name[0];
                            var sname = name[1];
                            var person = context.Persons.FirstOrDefault(x => x.FName == fname && x.SName == sname);
                            if (person == null)
                                person = new EFPerson() { FName = fname, SName = sname };
                            authors.Add(person);
                        }
                    }

                    var content = document.QuerySelector(".ar-detail");
                    var scripts = content.QuerySelectorAll("script");
                    foreach (var i in scripts)
                    {
                        i.Remove();
                    }

                    context.Articles.Add(new EFArticle()
                    {
                        Header = document.QuerySelector(".ar-detail h1").TextContent,
                        Url = new EFUrl() { Uri = page.CrawledPage.Uri.OriginalString },
                        Content = content.TextContent.Pretify(),
                        Date = DateTime.Parse(document.QuerySelector(".ar-date").TextContent),
                        Authors = authors
                    });
                    context.SaveChanges();
                }
                else if (page.CrawledPage.Uri.ToString().Contains(".cz/autori/") &&
                         document.QuerySelector(".ar-detail") != null &&
                         document.QuerySelector(".ar-list-description") != null)
                {
                    var rawName = document.QuerySelector(".ar-list-description").TextContent.Pretify().Split(' ');
                    if (rawName.Count() >= 2)
                    {
                        var fname = rawName[0];
                        var sname = rawName[1];

                        var person = context.Persons.FirstOrDefault(x => x.FName == fname && x.SName == sname);
                        if (person == null)
                        {
                            person = new EFPerson();
                            context.Persons.Add(person);
                        }

                        var detail = document.QuerySelector(".ar-detail");
                        var ps = detail.QuerySelectorAll("p");

                        person.FName = fname;
                        person.SName = sname;
                        person.Role = ps.First().TextContent.Split(' ').First();
                        if (ps.Length >= 2)
                        {
                            var content = ps[1];
                            var scripts = content.QuerySelectorAll("script");
                            foreach (var i in scripts)
                            {
                                i.Remove();
                            }
                            person.Desc = content.TextContent.Pretify();
                        }
                        context.SaveChanges();
                    }
                }
            }
        }

        private void crawler_ProcessPageCrawlCompleted(object sender, PageCrawlCompletedArgs e)
        {
            CrawledPage crawledPage = e.CrawledPage;

            if (crawledPage.WebException != null || crawledPage.HttpWebResponse.StatusCode != HttpStatusCode.OK)
                Console.WriteLine("Crawl of page failed {0}", crawledPage.Uri.AbsoluteUri);
            else
                Console.WriteLine("Crawl of page succeeded {0}", crawledPage.Uri.AbsoluteUri);

            if (string.IsNullOrEmpty(crawledPage.Content.Text))
                Console.WriteLine("Page had no content {0}", crawledPage.Uri.AbsoluteUri);

            //var htmlAgilityPackDocument = crawledPage.HtmlDocument; //Html Agility Pack parser
            var angleSharpHtmlDocument = crawledPage.AngleSharpHtmlDocument; //AngleSharp parser
            Extract(e, angleSharpHtmlDocument);
        }

        private void crawler_PageLinksCrawlDisallowed(object sender, PageLinksCrawlDisallowedArgs e)
        {
            CrawledPage crawledPage = e.CrawledPage;
            Console.WriteLine("Did not crawl the links on page {0} due to {1}", crawledPage.Uri.AbsoluteUri, e.DisallowedReason);
        }

        private void crawler_PageCrawlDisallowed(object sender, PageCrawlDisallowedArgs e)
        {
            PageToCrawl pageToCrawl = e.PageToCrawl;
            Console.WriteLine("Did not crawl page {0} due to {1}", pageToCrawl.Uri.AbsoluteUri, e.DisallowedReason);
        }
    }
}
